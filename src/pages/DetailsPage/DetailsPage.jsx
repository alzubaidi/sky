import React from 'react';
import { useParams } from 'react-router-dom';
import { Grid } from '@material-ui/core';

import { DetailsComponent } from 'components';

export default function DetailsPage() {
  const { type, id } = useParams();

  return (
    <Grid container spacing={1}>
      <Grid item xs={1} md={3}/>
      <Grid item xs={10} md={6}>
        <DetailsComponent type={type} id={id} />
      </Grid>
      <Grid item xs={1} md={3}/>
    </Grid>
  )
}
