export { default as HomePage } from './HomePage';
export { default as ResultsPage } from './ResultsPage';
export { default as DetailsPage } from './DetailsPage';
