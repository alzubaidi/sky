import React from 'react';
import { useParams } from 'react-router-dom';
import { Grid } from '@material-ui/core';

import { ResultsComponent } from 'components';

export default function Results() {
  const { type, term } = useParams();

  return (
    <Grid container>
      <Grid item xs={1}/>
      <Grid item xs={10}>
        <ResultsComponent term={term} type={type}/>
      </Grid>
      <Grid item xs={1}/>
    </Grid>);
}
