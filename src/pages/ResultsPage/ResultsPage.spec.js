import React from 'react';
import { HashRouter } from 'react-router-dom';
import { render, act } from '@testing-library/react';

import ResultsPage from './ResultsPage';

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({"page":1,"total_results":2,"total_pages":2,"results":[{"poster_path":"\/dXNAPwY7VrqMAo51EKhhCJfaGb5.jpg","popularity":61.697,"vote_count":17852,"video":false,"media_type":"movie","id":603,"adult":false,"backdrop_path":"\/fNG7i7RqMErkcqhohV2a6cV1Ehy.jpg","original_language":"en","original_title":"The Matrix","genre_ids":[28,878],"title":"The Matrix","vote_average":8.1,"overview":"Set in the 22nd century, The Matrix tells the story of a computer hacker who joins a group of underground insurgents fighting the vast and powerful computers who now rule the earth.","release_date":"1999-03-30"},{"vote_count":7209,"popularity":37.456,"id":604,"video":false,"media_type":"movie","vote_average":6.9,"title":"The Matrix Reloaded","release_date":"2003-05-15","original_language":"en","original_title":"The Matrix Reloaded","genre_ids":[12,28,53,878],"backdrop_path":"\/sDxCd4nt3eR4qOCW1GoD0RabQtq.jpg","adult":false,"overview":"Six months after the events depicted in The Matrix, Neo has proved to be a good omen for the free humans, as more and more humans are being freed from the matrix and brought to Zion, the one and only stronghold of the Resistance.  Neo himself has discovered his superpowers including super speed, ability to see the codes of the things inside the matrix and a certain degree of pre-cognition. But a nasty piece of news hits the human resistance: 250,000 machine sentinels are digging to Zion and would reach them in 72 hours. As Zion prepares for the ultimate war, Neo, Morpheus and Trinity are advised by the Oracle to find the Keymaker who would help them reach the Source.  Meanwhile Neo's recurrent dreams depicting Trinity's death have got him worried and as if it was not enough, Agent Smith has somehow escaped deletion, has become more powerful than before and has fixed Neo as his next target.","poster_path":"\/jBegA6V243J6HUnpcOILsRvBnGb.jpg"}]}),
    ok: true,
  })
);

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), 
  useParams: () => ({
    type: 'all',
    term: 'the matrix',
  }),
  useRouteMatch: () => ({ url: '/search/all/the matrix' }),
}));

describe('ResultsPage', () => {
  test('show results page', async () => {
    let result;

    await act(async () => {
     result = render(<HashRouter><ResultsPage /></HashRouter>);
    });

    expect(result.container).toMatchSnapshot();
  });
});
