import React from 'react';
import { render } from '@testing-library/react';

import HomePage from './HomePage';

test('renders notification message', () => {
  const { getByText } = render(<HomePage />);
  expect(getByText('Start by searching for a Movie or TV show above')).toBeInTheDocument();
});