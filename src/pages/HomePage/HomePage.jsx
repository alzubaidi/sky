import React from 'react';
import { Grid, Typography } from '@material-ui/core';

export default function Home() {
  return (
    <Grid container spacing={1}>
    <Grid item xs={1} md={3}/>
    <Grid item xs={10} md={6}>
      <Typography variant="h5" align="center">Start by searching for a Movie or TV show above</Typography>
    </Grid>
    <Grid item xs={1} md={3}/>
  </Grid>
  )
}
