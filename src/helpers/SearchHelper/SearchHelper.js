const baseAPI = process.env.REACT_APP_API_URL;
const defaultParams = {
  api_key: process.env.REACT_APP_API_KEY,
  lang: 'language=en-US',
  include_adult: false,
};

const getURL = (path, extraParams) => {
  const params = { ...defaultParams, ...extraParams };

  const url = new URL(`${baseAPI}${path}`);
  url.search = (new URLSearchParams(params));

  return url.href;
}

const search = async (type, query, page = 1) => {
  const searchURI = type === 'all' ? 'multi' : type;
  const url = getURL(`/search/${searchURI}`, { query, page });

  try {
    const response = await fetch(url);

    if(!response.ok) {
      throw new Error(`${response.status}: Failed to get results!`);
    }

    return await response.json();
  } catch({ message }) {
    return { error: true, message };
  }
}

const getMediaDetails = async (id, type) => {
  const url = getURL(`/${type}/${id}`);

  try {
   const response = await fetch(url);

    if(!response.ok) {
      throw new Error(`${response.status}: Failed to get results!`);
   }

    return await response.json();
  } catch({ message }) {
   return { error: true, message };
  }
}

const getMediaImages = async (id, type) => {
  const url = getURL(`/${type}/${id}/images`);

  try {
    const response = await fetch(url);
 
     if(!response.ok) {
       throw new Error(`${response.status}: Failed to get results!`);
    }
 
     return await response.json();
   } catch({ message }) {
    return { error: true, message };
   }
}

const getMediaCredits = async (id, type) => {
  const credits = type === 'person' ? 'combined_credits' : 'credits';
  const url = getURL(`/${type}/${id}/${credits}`);

  try {
    const response = await fetch(url);
 
     if(!response.ok) {
       throw new Error(`${response.status}: Failed to get results!`);
    }
 
     return await response.json();
   } catch({ message }) {
    return { error: true, message };
   }
}

export default {
  search,
  getMediaDetails,
  getMediaImages,
  getMediaCredits,
};
