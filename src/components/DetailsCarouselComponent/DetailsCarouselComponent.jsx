import React from 'react';
import PropTypes from 'prop-types';
import Carousel from 'react-material-ui-carousel';
import { Paper } from '@material-ui/core';

const Item = ({ title, file_path }) => {
  const image = `${process.env.REACT_APP_IMAGE_BASE}${file_path}`;
  return (
    <Paper align='center'>
      <img src={image} alt={title} title={title}/>
    </Paper>
  );
};

const getItems = (input, type) => {
  switch(type) {
    case 'person':
      return input.profiles;
    case 'movie':
    case 'tv':
      return input.posters;
    default:
      return []
  }
};

export default function DetailsCarouselComponent({ data, title, type }) {
  const items = getItems(data, type);

  if(!items) {
    return null;
  }

  return (
    <Carousel>
      {items.map((item, i) => <Item key={i} {...item} title={title} />)}
    </Carousel>
  );
};

DetailsCarouselComponent.propTypes = {
  data: PropTypes.object,
  title: PropTypes.string,
  type: PropTypes.string,
};
