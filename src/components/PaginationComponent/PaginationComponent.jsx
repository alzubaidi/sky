import React from 'react';
import PropTypes from 'prop-types';
import { Pagination } from '@material-ui/lab';

export default function PaginationComponent({ page, total, onPageChange }) {

  if(!total) {
    return null;
  }

  return (
    <div>
      <Pagination
        onChange={(_, page) => onPageChange(page)}
        variant="outlined"
        color="secondary"
        page={page}
        count={total}
      />
    </div>
  )
};

PaginationComponent.propTypes = {
  page: PropTypes.number,
  total: PropTypes.number,
  onPageChange: PropTypes.func,
};
