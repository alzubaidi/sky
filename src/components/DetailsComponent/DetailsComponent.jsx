import PropTypes from 'prop-types';
import { Alert } from '@material-ui/lab';
import React, { useEffect, useState } from 'react';
import { CircularProgress, Typography } from '@material-ui/core';

import { SearchHelper } from 'helpers';
import { DetailsCarouselComponent, DetailsCreditsComponent } from 'components';

export default function DetailsComponent({ id, type }) {
  const [details, setDetails] = useState({});
  const [status, setStatus] = useState({ loading: true, error: false });

  useEffect(() => {
    (async() => {
      setStatus({ loading: true });
      const result = await SearchHelper.getMediaDetails(id, type);
      const images = await SearchHelper.getMediaImages(id, type);
      const credits = await SearchHelper.getMediaCredits(id, type);
      setDetails({ ...result, images, credits });
      setStatus({ error: result.error, loading: false });
    })();
  }, [id, type]);

  if(status.loading) {
    return (
      <div style={{ textAlign: 'center' }}>
        <CircularProgress/>
      </div>
    );
  }

  if(status.error) {
    return (
      <div>
        <Alert severity="error" variant="filled">{details.message}</Alert>
      </div>
    );
  }

  const {
    title,
    name,
    overview,
    biography,
    birthday,
    release_date,
    credits,
    images,
  } = details;

  const header = title || name;
  const summary = overview || biography;

  return (
    <div>
      <Typography variant="h4" align='center'>{header}</Typography>
      <Typography variant="subtitle1" gutterBottom>{summary}</Typography>
      {birthday && <Typography variant="subtitle1" gutterBottom> Birthday: {birthday}</Typography>}
      {release_date &&
        <Typography variant="subtitle1" gutterBottom>
          Release Date: {release_date}
        </Typography>}
      <Typography variant="h6">
        <DetailsCreditsComponent data={credits} type={type} />
      </Typography>
      <Typography variant="h6">
        <DetailsCarouselComponent data={images} title={title} type={type} />
      </Typography>
  </div>
  );
}

DetailsComponent.propTypes = {
  id: PropTypes.string,
  type: PropTypes.oneOf(['movie', 'tv', 'person']),
};
