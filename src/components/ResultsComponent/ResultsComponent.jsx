import React, { useEffect, useState } from 'react';
import { Alert } from '@material-ui/lab';
import { Grid, CircularProgress, Typography, Container, makeStyles } from '@material-ui/core';

import { SearchHelper } from 'helpers';
import { ResultsListComponent, PaginationComponent } from 'components';

const useStyle = makeStyles((theme) => ({
  pagination: {
    margin: `${theme.spacing(2)} 0 ${theme.spacing(2)} 0`,
  }
}));

export default function ResultsComponent({ type, term }) {
  const classes = useStyle();
  const [result, setResult] = useState({});
  const [status, setStatus] = useState({ loading: true, error: false });
  const [page, setPage] = useState(1);

  const onPageChange = (page) => setPage(page);

  useEffect(() => {
    (async () => {
      setStatus({ loading: true });
      const data = await SearchHelper.search(type, term, page);
      setResult({ ...data });
      setStatus({ loading: false, error: data.error })
    })();
  }, [type, term, page]);

  const getContent = () => {
    const { results } = result;
    const { loading, error } = status;

    if(loading) {
      return <CircularProgress justify='center'/>
    }
  
    if(error) {
      return <Alert elevation={6} variant="filled" severity="error">{result.message}</Alert>;
    }
  
    if(!Array.isArray(results) || !results.length) {
      return (<Container>
          <Typography variant="h4" align='center'>No Data Found!</Typography>
          <Typography variant="subtitle1" align='center'>
          Your search for "{term}" has no results! Try to search again
          </Typography>
        </Container>);
    }

    return (<ResultsListComponent data={results} type={type}/>);
  }

  return (
    <Grid container>
      <Grid container justify='center'>
        {getContent()}
    </Grid>
    <Grid container justify='center'>
      <PaginationComponent
        className={classes.pagination}
        page={result.page}
        total={result.total_pages}
        onPageChange={onPageChange}
      />
    </Grid>
  </Grid>
  );
}
