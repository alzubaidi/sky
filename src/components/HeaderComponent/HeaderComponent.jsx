import React, { useRef, useEffect, useState, useCallback } from 'react';
import { debounce } from 'throttle-debounce';
import SearchIcon from '@material-ui/icons/Search';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
  Switch,
  FormControlLabel,
  Select,
  AppBar,
  TextField,
  IconButton,
  Toolbar,
  makeStyles,
} from '@material-ui/core';

import { SearchHelper } from 'helpers';

const useStyles = makeStyles((theme) => ({
  space: {
    marginLeft: theme.spacing(3),
  },
  toolbar: {
    justifyContent: 'center',
    alignContent: 'center',
  },
  autocomplete: {
    display: 'inline-flex',
    width: 255,
  }
}));

export default function Header({ history, switchTheme }) {
  const searchRef = useRef();
  const classes = useStyles();
  const [searchTerm, setSearchTerm] = useState('');
  const [suggestion, setSuggestions] = useState([]);
  const [searchType, updateSearchType] = useState('all');
  const [open, setOpen] = useState(false);

  const getSuggestion = debounce(400, useCallback(async () => {
    const data = await SearchHelper.search(searchType, searchTerm);
    if(data.error) {
      return;
    }

    const { results } = data;

    setSuggestions(results);
    
  }, [searchType, searchTerm]));

  useEffect(() => {
    searchRef.current.focus();
  }, []);

  useEffect(() => {
    if(searchTerm.length <5) {
      return;
    }
    getSuggestion(searchTerm);
  }, [searchTerm, getSuggestion]);

  const initSearch = (term) => {
    setOpen(false);
    history.push(`/search/${searchType}/${term}`);
  };

  return (
    <AppBar position="fixed">
      <Toolbar className={classes.toolbar}>
        <form onSubmit={() => initSearch(searchTerm)}>
        <Autocomplete
          open={open}
          onOpen={() => setOpen(true)}
          onClose={() => setOpen(false)}
          className={classes.autocomplete}
          options={suggestion}
          closeIcon={null}
          popupIcon={null}
          onChange={(_, value) => initSearch(value?.name || value?.title)}
          noOptionsText='Type five letters or more for suggestion'
          getOptionSelected={(option, value) =>
            (option.name || option.title) === (value?.name || value?.title)}
          getOptionLabel={(option) => option.name || option.title}
          renderInput={(params) => (<TextField
            {...params}
            inputRef={searchRef}
            placeholder="Search"
            onChange={({ target }) => setSearchTerm(target.value)}
            variant='outlined'
          />)}
        />
        <IconButton
          type="submit"
          className={classes.iconButton}
          aria-label="search"
          onClick={() => initSearch(searchTerm)}
        >
          <SearchIcon />
        </IconButton>
      </form>
      <Select native onChange={({ target }) => updateSearchType(target.value)}>
        <option value={'all'}>All</option>
        <option value={'movie'}>Movies</option>
        <option value={'tv'}>TV</option>
      </Select>
      <FormControlLabel control={<Switch color="default" onChange={switchTheme}/>} label="Dark"/>
      </Toolbar>
    </AppBar>
  );
}
