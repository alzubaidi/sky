import { withRouter } from 'react-router-dom';

import HeaderComponent from './HeaderComponent';

export default withRouter(HeaderComponent);
