export { default as HeaderComponent } from './HeaderComponent';
export { default as DetailsComponent } from './DetailsComponent';
export { default as ResultsListComponent } from './ResultsListComponent';
export { default as ResultsItemComponent } from './ResultsItemComponent';
export { default as DetailsCarouselComponent } from './DetailsCarouselComponent';
export { default as DetailsCreditsComponent } from './DetailsCreditsComponent';
export { default as PaginationComponent } from './PaginationComponent';
export { default as ResultsComponent } from './ResultsComponent';
