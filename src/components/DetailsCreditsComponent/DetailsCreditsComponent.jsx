import React from 'react';
import { Link } from 'react-router-dom';
import { Chip, Avatar } from '@material-ui/core';

const getItems = (input) => input.cast;
const getItemType = (type, mediaType) => type !== 'person' ? 'person' : mediaType;

export default function DetailsCreditsComponent({ data, type }) {
  const items = getItems(data);

  if(!items) {
    return null;
  }

  return items.map((item, i) => {
    const { id, profile_path, poster_path , title, name, media_type } = item;

    const linkType = getItemType(type, media_type);
    const label = title || name;
    const imagePath = profile_path || poster_path;

    const image = `${process.env.REACT_APP_IMAGE_BASE}${imagePath}`;
    const to = `/details/${linkType}/${id}`;

    return (
      <Chip
        key={i}
        variant="outlined"
        label={label}
        avatar={<Avatar src={image}/>}
        component={Link}
        to={to}
        clickable
      />
    );
  });
}
