import React from 'react'
import { Grid } from '@material-ui/core';

import { ResultsItemComponent } from 'components';

export default function ResultsListComponent({ data, type }) {
  return data.map((item) => (
    <Grid item sm={5} md={3} key={item.id}>
      <ResultsItemComponent data={item} type={type} />
    </Grid>
    ));
};
