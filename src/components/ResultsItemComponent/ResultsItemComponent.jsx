import React from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  Typography,
  CardContent,
  CardActionArea,
  CardMedia,
} from '@material-ui/core';
import { Rating } from '@material-ui/lab';

const getRating = (votes) => {
  const rate = Number.parseFloat(votes/2);

  if(!Number.isFinite(rate)) {
    return 0;
  }

  return Number(rate.toFixed(1));
}

export default function ResultsItemComponent({ data, type }) {
  const { id, title, name, vote_average, poster_path, media_type } = data;

  const header = title || name;
  const mediaType = media_type || type
  const imageURL = `${process.env.REACT_APP_IMAGE_BASE}${poster_path}`;

  return (
    <Card style={{height: '100%'}}>
      <CardActionArea component={Link} to={`/details/${mediaType}/${id}`}>
        <CardMedia
          component="img"
          height="150"
          image={imageURL}
          alt={header}
          title={header}
        />
      <CardContent>
          <Typography gutterBottom variant="h5">{header}</Typography>
          <Typography>
            <Rating name="half-rating-read" value={getRating(vote_average)} precision={0.1} readOnly />
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
