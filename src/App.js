import React, { useState } from 'react';
import { Grid, CssBaseline } from '@material-ui/core';
import { Route, Switch, HashRouter } from 'react-router-dom';
import { ThemeProvider, createMuiTheme, makeStyles } from '@material-ui/core/styles';

import { HeaderComponent } from 'components';
import { HomePage, ResultsPage, DetailsPage } from 'pages';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  content: {
    marginTop: theme.spacing(10),
  }
}));

const App = () => {
  const classes = useStyles();
  const [isDark, setDark] = useState(false);

  const theme = createMuiTheme({
    palette: {
      type: isDark ? 'dark' : 'light',
    },
    spacing: 5,
  });

  const switchTheme = () => setDark(!isDark);

  return (
    <ThemeProvider theme={theme}>
    <CssBaseline />
    <div className={classes.root}>
      <HashRouter>
      <HeaderComponent switchTheme={switchTheme} />
      <Grid container spacing={2} className={classes.content}>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/search/:type/:term">
          <ResultsPage />
        </Route>
        <Route exact path="/details/:type/:id">
          <DetailsPage />
        </Route>
      </Switch>
      </Grid>
      </HashRouter>
    </div>
    </ThemeProvider>
  );
};

export default App;
