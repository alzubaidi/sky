import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText('Start by searching for a Movie or TV show above');
  expect(linkElement).toBeInTheDocument();
});
