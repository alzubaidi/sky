## Sky Test

Summary
---
The test is based on "create-react-app" where components and pages added.
  
**Running**
---
 1. Install dependancies: `npm i`
 2. Start dev server locally: `npm start`
 3. Browser instance/tab should be opened with app loaded. if not, head to http://localhost:3000/
 4. To run unit-tests: `npm test`

**Notes**
---
- For simplicity, test was done using client-side only. usually, you would have a server-side were API key and other info is masked from the client.
- Due lack of time, tests stitched really quick as well as other improvements that can be made.
